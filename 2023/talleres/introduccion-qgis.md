---
layout: 2023/post
section: proposals
category: workshops
author: Carlos López Quintanilla
title: Introducción a QGIS
---

# Introducción a QGIS

>QGIS es un programa de software libre de SIG, Sistemas de Información Geográfica. QGIS permite cargar muchos formatos de capas vectoriales y ráster. Tiene muchísimas herramientas para el análisis espacial y un espacio para el diseño de composiciones o mapas.<br><br>
Comenzará con una charla que pretende explicar para qué sirve y que características tiene este programa, seguido de un taller práctico de uso de las herramientas más básicas de QGIS para empezar a trabajar. Se cargarán capas vectoriales y ráster, se modificarán sus propiedades de simbología. Y finalmente, se creará una composición para exportar a PDF un mapa.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>QGIS es un Sistema de Información Geográfica (SIG) de Código Abierto licenciado bajo GNU - General Public License. QGIS es un proyecto oficial de Open Source Geospatial Foundation (OSGeo). Corre sobre Linux, Unix, Mac OSX, Windows y Android y soporta numerosos formatos y funcionalidades de datos vector, datos ráster y bases de datos.<br><br>
La comunidad<br><br>
QGIS es un proyecto impulsado por voluntarios. Damos la bienvenida a contribuciones en la forma de contribuciones de código, corrección de errores, reportes de errores, aportes de documentación, apoyo y soporte a otros usuarios en nuestras listas de correos y gis.stackechange.com. Si estás interesado en apoyar activamente el proyecto, puedes encontrar más información bajo el menú desarrollo y en la Wiki de QGIS.<br><br>
Características<br><br>
QGIS proporciona una creciente gama de capacidades a través de sus funciones básicas y complementos. Puede visualizar, gestionar, editar y analizar datos, y diseñar mapas imprimibles. Obtenga una primera impresión con una lista más detallada de características.

-   Web del proyecto: <https://www.qgis.org>

-   Público objetivo:

>Un programa de SIG (Sistema de Información Geográfica) puede ser utilizado por una amplia variedad de personas y organizaciones, tales como:<br><br>
Gobiernos y organismos públicos: para el manejo y análisis de información geoespacial relacionada con la planificación territorial, la gestión ambiental, la gestión de riesgos, la seguridad ciudadana, entre otros.<br><br>
Empresas privadas: para la toma de decisiones relacionadas con la gestión de recursos naturales, la planificación de rutas de transporte y logística, la ubicación de instalaciones comerciales, la identificación de oportunidades de negocio y la evaluación de riesgos.<br><br>
Investigadores y académicos: para el análisis y visualización de datos geográficos en diferentes áreas de estudio, tales como la ecología, la geografía, la sociología, la salud pública y la arqueología.<br><br>
Organizaciones no gubernamentales: para el monitoreo y evaluación de proyectos de conservación, el análisis de conflictos territoriales, la planificación de actividades...<br>

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Sábado 13 de mayo de 12:00 a 14:00 en el Aula 4 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/0a05281a-a6dc-48ee-b354-63d239d26ee8>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Carlos López Quintanilla

-   Bio:

>Consultor SIG desde hace más de 23 años, con amplia experiencia en implementación de SIG a multitud de entidades y empresas. Actualmente, es presidente de la asociación de QGIS España y colabora con el proyecto QGIS desde hace más de 10 años.

### Info personal:

-   Web personal: <https://www.qgis.es/>
-   Twitter: <https://twitter.com/@qgises>
-   GitLab (u otra forja) o portfolio general: <https://github.com/qgis/QGIS>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
