---
layout: 2023/post
section: proposals
category: workshops
author: Antonio Montes (inventadero)
title: Edición del Paisaje en realidad extendida (XR)
---

# Edición del Paisaje en realidad extendida (XR)

>Los espacios de realidad aumentada o virtual no tienen por qué pertenecer a corporaciones exclusivamente. En este terreno hay mucho trabajo de desarrollo FLOSS XR. Creo interesante que intervengamos en estos paisajes públicos de modo colaborativo.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Taller práctico 2 h para crear un paisaje 3D con la topografía de Zaragoza, interviniendo en ella y finalmente exportándola a un un espacio virtual de mozilla hubs.<br><br>
Como propuesta inicial está seguir algunos de los pasos descritos en la investigación abierta  sobre Malagueira XR (propuesta para Charla) como prototipo del futuro Recurso Educativo Abierto: “HACK FOR XR - Heritage Art Creation Kit; Free Open Resources; Extended Reality”. Donde compartir los jallos descubiertos y poder reproducir de los procesos que resultaron efectivos, señalando sus limitaciones, necesidades (como conocer posibilidades de OSM) y retos para el futuro.<br><br>
Lista de software susceptible de utilizarse: F3D, Blender (GIS addon), Meshlab, CloudCompare, Spoke (online), OpenBrush (VR o Monoscope) ... y el que se te ocurra.<br><br>
Taller también abierto en su planteamiento, y espero que variado en su desarrollo al enriquecerse con aportaciones de las personas participantes ~ ~ ~  

-   Web del proyecto: <https://8d2.es/course/view.php?id=79>

-   Público objetivo:

>Interesados en XR, topografía, dibujo, edición 3D, arquitectura, avatares, naturaleza ~ ~ ~

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Viernes 12 de mayo de 12:00 a 14:00 en el Aula 3 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/2d3e587b-e4dc-42dd-adcf-c06e3f0f6b13>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Antonio Montes [inventadero]

-   Bio:

>Dibujante, "empantallao" a veces, Blenderita , enseñando~aprendiendo.

### Info personal:

-   Web personal: <https://8d2.es/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@inventadero>
-   Twitter: <https://twitter.com/@inventadero>
-   GitLab (u otra forja) o portfolio general: <https://github.com/inventadero>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
