---
layout: 2023/post
section: proposals
category: talks
author: Víctor Martínez
title: Herramientas libres de un SIG con Información Geografica Voluntaria
---

# Herramientas libres de un SIG con Información Geografica Voluntaria

>Charla sobre herramientas libres sobre sistemas de información geográfica usando PostgresSQL, Apache, Docker, GeoServer, Leaflet, Turf...

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Repaso de las herramientas libres necesarias para hacer un SIG con información geografica voluntaria. Revisión de algunos ejemplos.

-   Web del proyecto: <https://github.com/vmbolea/GeoAgenda-Cultural>

-   Público objetivo:

>Personas usuarias de herramientas libres y personas curiosas en general

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 16:45-17:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Víctor Martínez

-   Bio:

>Técnico Medio Ambiente - Técnicos Sistemas de Información Geográfica

### Info personal:

-   Web personal: <https://www.linkedin.com/in/v%C3%ADctor-mart%C3%ADnez-bolea-556b8a135/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
