---
layout: 2023/post
section: proposals
category: talks
author: Maria Caudevilla Lambán
title: Metodología libre para la elaboración de cartografía turística
---

# Metodología libre para la elaboración de cartografía turística

>Los mapas turísticos son la tipología de mapas más empleada por el público general, y elaborarlos mediante procesos colaborativos entre los diferentes agentes del territorio involucrados puede resultar diferenciador para la planificación turística de los destinos.<br><br>
Unido al empleo de Sistemas de Información Geográfica (QGIS) y programas de diseño vectorial (Inkscape) de código abierto, el resultado cartográfico generado muestra un enorme potencial no solo como herramienta de planificación territorial sino como mapa de información para los visitantes.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>El uso de software libre es una manera eficaz de acometer proyectos de planificación turística territorial.
Esta charla pretende ilustrar las fases del procedimiento de software libre QGIS – Inkscape y el proceso colaborativo seguido entre los agentes del territorio involucrados para la elaboración de una cartografía turística inteligente. Desde un punto de vista objetivo, se contará la experiencia con el procedimiento y se mostrará el resultado final obtenido.<br><br>
Dada la eficacia de la metodología libre para acometer proyectos de esta temática, existe un importante esfuerzo de divulgación de la misma, que queda plasmado de manera científica en revistas de acceso abierto – Open Access – y mediante docencia universitaria en cursos de tratamiento de datos espaciales y diseño cartográfico.

-   Público objetivo:

>Profesionales del sector y cualquier persona interesada en software libre.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 17:00-17:15
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Maria Caudevilla Lambán

-   Bio:

>Doctoranda de la Universidad de Zaragoza en el Programa de Ordenación del Territorio y Medio Ambiente y miembro del Grupo de Estudios en Ordenación del Territorio (GEOT).

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
