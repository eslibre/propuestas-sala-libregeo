---
layout: 2023/post
section: proposals
category: talks
author: yopaseopor
title: OSMPoisMap y Tagcionario&#58; acercando las etiquetas al público general
---

# OSMPoisMap y Tagcionario: acercando las etiquetas al público general

> OSMPoisMap es un mapa personalizable que apunta hacia ciertas etiquetas. Con el tagcionario puedes saber qué etiquetas necesitas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Remoto
-   Idioma: Español

-   Descripción:

>1. Los Poi son una de las fuerzas de OSM.
2. OSMPOISMap permite ver esos poi de manera más o menos gráfica
3. Para clasificar esos pois necesitamos etiquetas
4. Tagcionario: cuando no hay tiempo de consultar qué etiquetas necesitas

-   Web del proyecto: <https://yopaseopor.github.io/osmpoismap>

-   Público objetivo:

>Público en general y noveles avanzados con OSM con conocimientos iniciales de código (entender y copiar)

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 16:00-16:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: yopaseopor

-   Bio:

>Desde 2011 formo parte de OSM a través de la comunidad catalana y española, mis intereses derivan hacia acercar las posibilidades personalizables que nos ofrece OSM al público en general y a los noveles un poco avanzados en particular.

### Info personal:

-   Web personal: <https://yopaseopor.blogspot.com/>
-   Mastodon (u otras redes sociales libres): <https://mapstodon.space/@yopaseopor>
-   Twitter: <https://twitter.com/@yopaseopor>
-   GitLab (u otra forja) o portfolio general: <https://github.com/yopaseopor>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
