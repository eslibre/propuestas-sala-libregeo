---
layout: 2023/post
section: proposals
category: talks
author: Rafael Martínez Cebolla
title: Repositorio de código abierto de ICEARAGON
---

# Repositorio de código abierto de ICEARAGON

>Charla explicando el conjunto de servicios web geográficos abiertos publicados por IGEAR a través de la plataforma ICEARAGON. La motivación radica en su difusión y conocimiento a la Comunidad libre para que puedan ser reutilizados o mejorados.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La charla introduce el conjunto de servicios y aplicaciones web geográficas que el IGEAR ha desarrollado en el último lustro y que pueden ser usados de forma libre y gratuita. Se detallará cuáles son las razones que motivan el trabajo por parte del IGEAR de desarrollar soluciones libres de pago para su uso por cualquier actor público o privado.<br><br>
La charla contará a través de la experiencia de implantación de los servicios de ICEARAGON cuál es la justificación de invertir esfuerzos humanos, técnicos y económicos para el apoyo a la toma de decisiones de las políticas públicas del Gobierno de Aragon.<br><br>
La charla distinguirá caso y ejemplos de uso tanto de los servicios web como de las aplicaciones web geográficas que usan y implementan esos servicios sea el caso de Donde Vivo, Atlas, Tabla a Mapa o Visor OTINFO a través de servicios web como SimpleSearchService o SpatialSearchService.<br><br>
Se cerrará la charla con lo que un proyecto como ICEARAGON revierte a la sociedad en términos económicos (y éticos) para entender la dimensión de la utilidad que tiene invertir en código abierto.

-   Web del proyecto: <https://gitlab.com/igear_publico>

-   Público objetivo:

>Desarrolladores web y cualquier actor público o privado que necesite implementar servicios web geográficos.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 16:00-16:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Rafael Martínez Cebolla

-   Bio:

>Geógrafo en [@GobAragon](https://twitter.com/GobAragon) en [@IGEAR_Aragon](https://twitter.com/IGEAR_Aragon) y Presidente del Colegio Geógrafos de Aragón [@GeografosAragon](https://twitter.com/GeografosAragon).

### Info personal:

-   Web personal: <https://www.linkedin.com/in/rafael-mart%C3%ADnez-cebolla-99238197/>
-   Mastodon (u otras redes sociales libres): <https://indieweb.social/web/@galactero>
-   Twitter: <https://twitter.com/@galactero>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/igear_publico>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
