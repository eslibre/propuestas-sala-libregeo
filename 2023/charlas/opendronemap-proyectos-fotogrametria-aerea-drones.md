---
layout: 2023/post
section: proposals
category: talks
author: Joan Cano Aladid
title: El uso de OpenDroneMap en proyectos de fotogrametría aérea con drones
---

# El uso de OpenDroneMap en proyectos de fotogrametría aérea con drones

>Realizar un levantatamiento topográfico mediante fotogrametría aérea requiere siempre de un procesamiento de las imágenes. Para ello, existen en el mercado multitud de herramientas que nos permiten hacerlo, también, existen alternativas libres. Durante la charla, veremos una de las aplicaciones libres más utilizadas y que más ha crecido en los últimos años, OpenDroneMap.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

> Durante la charla se explicarán los conceptos básicos para realizar fotogrametría con drones; la instalación y el uso de OpenDroneMap, WebODM y el procesamiento de conjuntos de datos. También, los productos que se generan y cómo se pueden utilizar en otras aplicaciones libres espaciales.

-   Web del proyecto: <https://opendronemap.org/>

-   Público objetivo:

>Cartógrafos, arquitectos, ingenieros

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 18:45-19:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Joan Cano Aladid

-   Bio:

>Joan Cano es graduado en GOT y máster en TIG. Desde que finalizó sus estudios ha estado desarrollándose profesionalmente en una empresa referente en el sector de la topografía y geomática. Actualmente desarrolla su propio proyecto Liberam, desde el que aporta soluciones cartográficas apoyadas en el uso de drones y láser escáner.

### Info personal:

-   Web personal: <https://liberam.es>
-   Twitter: <https://twitter.com/Joan_C_A>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
