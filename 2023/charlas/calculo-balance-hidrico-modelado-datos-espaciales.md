---
layout: 2023/post
section: proposals
category: talks
author: Jose Lozano
title: Cálculo de balance hídrico a través del modelado de datos espaciales, en la cuenca del río Motatán, Estado Trujillo
---

# Cálculo de balance hídrico a través del modelado de datos espaciales, en la cuenca del río Motatán, Estado Trujillo

>Charla sobre calcular aspectos como el balance hídrico mediante modelamiento de datos geoespaciales y SIG.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Remoto
-   Idioma: Español

-   Descripción:

>En el presente trabajo se expone el proceso metodológico para calcular el balance hídrico promedio mensual para una cuenca hidrográfica localizada en los Paramos de Motatan y se extiende hacia el estado Trujillo, el objetivo fue evaluar la importancia de la dinámica hídrica. El procedimiento utiliza herramientas SIG para elaborar el balance hídrico superficial de acuerdo a la metodología clásica de Thornthwaite, la cual permite calcular las variables del sistema hídrico a partir de la definición de la capacidad máxima de agua disponible en el terreno (CAD), las medidas (promedio o totales) de precipitación y del estimado de la evapotranspiración potencial siguiendo la metodología propuesta por Thornthwaite.<br><br>
En el ambiente de SIG-Qgis fue posible realizar el balance hídrico aplicando álgebra de mapas a superficies continuas. Los productos intermedios y finales de este ejercicio de simulación son capas ráster o matrices espacio-temporales que representan la condición hídrica promedio.

-   Público objetivo:

>Profesionales, técnicos, estudiantes.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 17:15-17:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Jose Lozano

-   Bio:

>Ingeniero Agronomo con experiencia en proyectos agronómicos, agrícolas, hidráulicos, ambientales, bajo ambiente GIS y modelado de datos espaciales.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
