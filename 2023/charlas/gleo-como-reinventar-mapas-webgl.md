---
layout: 2023/post
section: proposals
category: talks
author: Iván Sánchez Ortega
title: Gleo, o cómo reinventar mapas con WebGL
---

# Gleo, o cómo reinventar mapas con WebGL

>Gleo es una librería JavaScript+WebGL para visualización de mapas (similar a Leaflet/OpenLayers/MapLibre) publicada bajo GPL. Se verán motivación, arquitectura, y resultados.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Gleo intenta cubrir un nicho en las librerías de visualización de mapas en web. Intenta tener la extensibilidad (orientada a objetos) de Leaflet, el soporte de proyecciones de OpenLayers y la eficiencia WebGL de MapLibre.<br><br>
En esta sesión se verá, a grandes rasgos, la arquitectura de Gleo y las técnicas que implementa para lidiar con problemas de reproyección; la extensibilidad de las clases que implementan símbolos gráficos; y el manejo de renderizado de símbolos a campos escalares.

-   Web del proyecto: <https://gitlab.com/IvanSanchez/gleo/>

-   Público objetivo:

>Gente que conozca un poquito de JavaScript y mapas.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 17:30-18:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Iván Sánchez Ortega
-   Bio:

>Yo antiguamente escribía en es.comp.os.linux y asistía a las HispaLinux. Ahora soy miembro de OSGeo y programo cosas con demasiado JavaScript.

### Info personal:

-   Web personal: <https://ivan.sanchezortega.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@IvanSanchez>
-   Twitter: <https://twitter.com/@realivansanchez>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/IvanSanchez/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
