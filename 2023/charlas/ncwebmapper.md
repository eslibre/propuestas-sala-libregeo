---
layout: 2023/post
section: proposals
category: talks
author: Fergus Reig Gracia
title: NcWebMapper
---

# NcWebMapper

>NcWebMapper es una librería que genera una página web de mapas a partir de ficheros NetCDF con información temporal y espacial.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>NcWebMapper es una librería que creamos para las necesidades de la PTI+CLima del CSIC donde solemos construir webs desde R a partir de ficheros NetCDF en las que se puede consultar la información de un px a través del tiempo pero también visualizar en un mapa toda la información disponible para una sola fecha. Después de hacer las primeras 3 páginas de este tipo decidimos automatizarlo en una librería.

-   Web del proyecto: <https://github.com/ncwebmapper/ncwebmapper>

-   Público objetivo:

>Humanos a los que les pueda interesar la librería o su funcionamiento.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 18:30-18:45
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Fergus Reig Gracia

-   Bio:

>Creo que es mi primera charla, he participado en el desarrollo del paquete al 50%, pero no controlo del todo todo lo que realiza, la otra persona que participó irá como apoyo.<br><br>
Analista programador de la PTI+Clima del CSIC. Afantástico, pastafari, tecnooptimista, dataísta, tranhumanista, explorador, defensor del sistema público y del software libre, fundador del Colegio de Ingeniería Informática de Aragón, fundador de Makeroni / The IF, excandidato al Congreso por el Partido Pirata, esposo, cinéfilo, coleccionista, monolingüe... siempre soñando con fusionar el mundo digital con el que está alejado del teclado.

### Info personal:

-   Web personal: <http://fergusreig.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@MuyDestructor>
-   Twitter: <https://twitter.com/MuyDestructor>
-   GitLab (u otra forja) o portfolio general: <https://github.com/MuDestructor>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
