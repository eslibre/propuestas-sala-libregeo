---
layout: 2023/post
section: proposals
category: talks
author: Carmen Díez SanMartín
title: Las calles de la mujeres en Aragón por Geochicas OSM
---

# Las calles de la mujeres en Aragón por Geochicas OSM

>'Las calles de las mujeres' es un proyecto colaborativo para producir un mapa a partir de las calles que cuenten con el nombre de alguna mujer. Busca enlazar y generar contenidos en OSM y Wikipedia sobre mujeres destacadas y así visibilizar la brecha de género que existe históricamente dentro de las ciudades. En Aragón se ha elaborado en Zaragoza y Huesca.<br><br>
GeochicasOSM es un grupo de mujeres que realizan mapeo en OpenStreetMap y trabajan con software libre. El grupo, que es abierto y participativo (no es exclusivo), fue creado en la Conferencia ‘SOTM LATAM 2016’, en São Paulo (Brasil) y actualmente hay usuarias en al menos 20 países de 3 continentes diferentes, entre ellos España.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Las #CallesDeLasMujeres es un proyecto de ‘GeoChicasOSM’, que surgió en el marco del 8M de 2018. Este proyecto busca dar visibilidad a la disparidad de género que hay en la nomenclatura de las calles a la vez que promueve la movilización de distintas comunidades, como Wikipedia, y el debate sobre los roles de las mujeres históricamente y sus aportes socio-culturales.<br><br>
Para ello se ha creado un mapa mundial donde, para algunas ciudades de América Latina y España, se han marcado las calles con nombre de hombre y las calles con nombre de mujer, permitiendo hacer un análisis comparativo y ver qué porcentaje representa cada grupo. Posteriormente se ha enlazado cada calle con nombre de mujer con su correspondiente artículo en Wikipedia, detectando así cuántas de ellas ni siquiera aparecen.<br><br>
El proceso técnico, desde la obtención y tratamiento de los datos, hasta la elaboración del mapa para su visualización, se resume de la siguiente manera:<br><br>
   - Obtención de datos iniciales (Open Street Map)<br>
   - Tratamiento de datos: filtrado de elementos y obtención del listado de nombres de las calles<br>
   - Clasificación por género<br>
   - Asociación con su entrada en Wikipedia<br>
   - Generación de fichero final de resultados<br>
   - Desarrollo del visor donde se cargan los datos y estilización, per medio de la librería Mapbox GL JS.<br><br>
La elaboración del proyecto tanto en Zaragoza como en Huesca fue una experiencia estupenda, tanto para el conocimiento de herramientas libres como para la creación de comunidad y concienciación de la poca visibilidad de la mujer en algo tan histórico como el nombre de sus calles.

-   Web del proyecto: <https://geochicas.org/index.php/que-hacemos/proyectos/las-calles-de-las-mujeres/>

-   Público objetivo:

>Todas las personas sensibilizadas con el uso de datos libres y herramientas open, a las que además les guste formar parte de comunidades de trabajo y compartir proyectos, creando sinergias.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 16:30-16:45
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Carmen Díez SanMartín

-   Bio:

>Soy Ingeniera Civil y Máster en Ingeniería Ambiental y Gestión del Agua. Trabajo en la administración pública, concretamente en la Consejería de Medio Ambiente de la Comunidad de Madrid utilizando GIS en la planificación general del desarrollo urbano y espacial, actualizando geodatabases espaciales con información de planificación urbana.<br><br>
Pertenezco desde hace algunos años a varios grupos que apoyan el desarrollo de Open Sources y Software Libre (Geoinquietos Madrid, AESIG, GeochiscasOSM y QGIS España) e intento contribuir al uso de Sistemas de Información Geográfica gratuitos y la creación de bases de datos abiertas, así como a la participación en eventos de mapeo humanitario (WaterHack, Mapeado Colaborativo en mapatones con Missing Maps, OSM y HOT), desarrollo local (Accessibility Mapping Party y Geocamp) y difusión internacional (FOSS4G y QGIS User Conference and Hackmeeting).

### Info personal:

-   Web personal: <https://wiki.osgeo.org/wiki/User:Clds1>
-   Twitter: <https://twitter.com/@carmen10maica>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
